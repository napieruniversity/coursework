# README #

This repository contains source code files for CSN09112 Network Security and Cryptography coursework

### What is this repository for? ###

* Source files not included with submitted coursework
* Version 1.0


### How do I get set up? ###


* Configuration

To run the crack.py script you will need a python interpreter and cryptodome package

* Dependencies

Install python cryptodome package and a text file with dictionary words or common passwords. Replace
the text file path in source file.


