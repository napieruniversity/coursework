#!/usr/bin/env python
 
import socket
import time   


'''
    Script to test the response of C&C server located at 87.106.189.123. 
    Performs the same command iteration a number of times and adds all responses to a set. 
    Useful to establish the number of different commands received by the C&C server.
'''



TCP_IP = '87.106.189.123'
TCP_PORT = 5001
BUFFER_SIZE = 64000
HELLO = "HELLO\n"
HELLO_RESP = "I am okay"
aSet = set()
   
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
s.send(HELLO)
data = s.recv(BUFFER_SIZE)
if (len(data) == 0):
    print 'Wrong response from HELLO'
    s.close()
    exit(1)
'''
Change this to suit. Options used in bot.exe:
      "TEST",
      "FAILOVER 11672634",
      "CONNECT",
      "TAKEDOWN",
      "CAPTURE",
      "KEEPALIVE",
      "LOOP",
      "LOOK",
      "CODE",
      "GENERATE",
      "GET"
''' 
aStringToSend = 'KEEPALIVE\n'
aCounter = 0
while (aCounter < 1000):
    s.send(aStringToSend)
    data = s.recv(BUFFER_SIZE)
    if len(data) != 0:
        aSet.add(data)
    time.sleep(1)
    aCounter += 1
    print 'Response', aCounter, ':', data
s.close()
print 'Got', len(aSet), 'responses'
for i in aSet:
    print i
print "received data:", data
print 'End', len(aSet), 'responses'