
# sudo -H pip install pycryptodome
from Crypto.Hash import HMAC, SHA512, BLAKE2b, SHA
from Crypto.Cipher import AES, Salsa20, DES3, ARC4, ARC2, Blowfish, PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto import Random
from base64 import b64decode
from binascii import hexlify, unhexlify
import sys
import string

# 1. Decode to raw bytes
# The message? 0e0130c44554d69ff16d2afc2368e277a89907c09866025531954c4926f0b60cf2837000c476e640b8e87ca51fbd5ce285cf54df82cd58a409bed749578d892a in b64
aMessage = b64decode("DgEwxEVU1p/xbSr8I2jid6iZB8CYZgJVMZVMSSbwtgzyg3AAxHbmQLjofKUfvVzihc9U34LNWKQJvtdJV42JKg==")

# The key? 029daf4d5b069a5ab890703a8cbf1949 in b64
aKey = b64decode("Ap2vTVsGmlq4kHA6jL8ZSQ==")

# Test HMAC512 with aKey being the secret key and candidate being the plaintext message to authenticate
def testHMAC512(candidate):
    aHMAC = HMAC.new(aKey, digestmod=SHA512)
    aHMAC.update(aCandidate)
    try:
        aHMAC.verify(aMessage)
        print "The message '%s' is authentic" % aItem
        print "Output from HMAC function:", aHMAC.hexdigest()
        exit(0)
    except ValueError:
        pass

# Test Blake2b with aKey being the secret key and candidate being the plaintext message to authenticate
def testBLAKE2b(candidate):
    aMac = BLAKE2b.new(digest_bits=512, key=aKey)
    aMac.update(candidate)
    if aMac.hexdigest() == '0e0130c44554d69ff16d2afc2368e277a89907c09866025531954c4926f0b60cf2837000c476e640b8e87ca51fbd5ce285cf54df82cd58a409bed749578d892a':
        print "===================================================================="
        print "Found IT", aMac.hexdigest(), str(candidate)
        print "===================================================================="
        exit(0)
    

# Test AES128
def testAES(candidate):
    # 1. ECB mode, no iv
    cipher = AES.new(aKey, AES.MODE_ECB)
    plaintext = cipher.decrypt(aMessage)
    if all(c in string.printable for c in plaintext):
        print "===============================", "AES MODE_ECB", "==============================="
        print "candidate:", candidate
        print "plaintext:", plaintext
        exit(0)
    # 2. CBC Mode, iv has to be 16 bytes.
    # 2.1. Use candidate as iv and aKey as secret key
    if len(candidate) == 16:
        cipher = AES.new(aKey, AES.MODE_CBC, iv=candidate)
        plaintext = cipher.decrypt(aMessage)
        if all(c in string.printable for c in plaintext):
            print "===============================", "AES MODE_CBC", "==============================="
            print "candidate:", candidate
            print "plaintext:", plaintext
            exit(0)
    # 2.2. Use aKey as iv and candidate as secret key (16 or 32 bytes since we are alreay here)
    if (len(candidate) == 16) or (len(candidate) == 32):
        cipher = AES.new(candidate, AES.MODE_CBC, iv=aKey)
        plaintext = cipher.decrypt(aMessage)
        if all(c in string.printable for c in plaintext):
            print "===============================", "AES MODE_CBC", "==============================="
            print "candidate:", candidate
            print "plaintext:", plaintext
            exit(0)
    # 3. CFB Mode, iv has to be 16 bytes
    # 3.1. Use candidate as iv and aKey as secret key
    if len(candidate) == 16:
        cipher = AES.new(aKey, AES.MODE_CFB, iv=candidate)
        plaintext = cipher.decrypt(aMessage)
        if all(c in string.printable for c in plaintext):
            print "===============================", "AES MODE_CFB", "==============================="
            print "candidate:", candidate
            print "plaintext:", plaintext
            exit(0)
    # 3.2. Use aKey as iv and candidate as secret key (16 or 32 bytes since we are alreay here)
    if (len(candidate) == 16) or (len(candidate) == 32):
        cipher = AES.new(candidate, AES.MODE_CFB, iv=aKey)
        plaintext = cipher.decrypt(aMessage)
        if all(c in string.printable for c in plaintext):
            print "===============================", "AES MODE_CFB", "==============================="
            print "candidate:", candidate
            print "plaintext:", plaintext
            exit(0)
    # 4. OFB Mode, iv has to be 16 bytes
    # 4.1. Use candidate as iv and aKey as secret key
    if len(candidate) == 16:
        cipher = AES.new(aKey, AES.MODE_OFB, iv=candidate)
        plaintext = cipher.decrypt(aMessage)
        if all(c in string.printable for c in plaintext):
            print "===============================", "AES MODE_OFB", "==============================="
            print "candidate:", candidate
            print "plaintext:", plaintext
            exit(0)
    # 4.2. Use aKey as iv and candidate as secret key (16 or 32 bytes since we are alreay here)
    if (len(candidate) == 16) or (len(candidate) == 32):
        cipher = AES.new(candidate, AES.MODE_OFB, iv=aKey)
        plaintext = cipher.decrypt(aMessage)
        if all(c in string.printable for c in plaintext):
            print "===============================", "AES MODE_OFB", "==============================="
            print "candidate:", candidate
            print "plaintext:", plaintext
            exit(0)
    '''
        Counter mode
    ''' 
    # 5. EAX Mode, no restriction on nonce length (recommended is 16 bytes)
    # 5.1. Use candidate as iv and aKey as secret key
    cipher = AES.new(aKey, AES.MODE_EAX, nonce=candidate)
    plaintext = cipher.decrypt(aMessage)
    if all(c in string.printable for c in plaintext):
        print "===============================", "AES MODE_EAX", "==============================="
        print "candidate:", candidate
        print "plaintext:", plaintext
        exit(0)
    # 5.2. Use aKey as iv and candidate as secret key (16 or 32 bytes since we are alreay here)
    if (len(candidate) == 16) or (len(candidate) == 32):
        cipher = AES.new(candidate, AES.MODE_OFB, iv=aKey)
        plaintext = cipher.decrypt(aMessage)
        if all(c in string.printable for c in plaintext):
            print "===============================", "AES MODE_EAX", "==============================="
            print "candidate:", candidate
            print "plaintext:", plaintext
            exit(0)
    # 6. GCM Mode, no restriction on nonce length (recommended is 16 bytes)
    # 6.1. Use candidate as iv and aKey as secret key
    cipher = AES.new(aKey, AES.MODE_GCM, nonce=candidate)
    plaintext = cipher.decrypt(aMessage)
    if all(c in string.printable for c in plaintext):
        print "===============================", "AES MODE_GCM", "==============================="
        print "candidate:", candidate
        print "plaintext:", plaintext
        exit(0)
    # 6.2. Use aKey as iv and candidate as secret key (16 or 32 bytes since we are alreay here)
    if (len(candidate) == 16) or (len(candidate) == 32):
        cipher = AES.new(candidate, AES.MODE_GCM, nonce=aKey)
        plaintext = cipher.decrypt(aMessage)
        if all(c in string.printable for c in plaintext):
            print "===============================", "AES MODE_GCM", "==============================="
            print "candidate:", candidate
            print "plaintext:", plaintext
            exit(0)
    

# Test XSalsa20 mode with aKey being the nonce. Requirements: nonce must be 8 bytes
def testXSalsa(candidate):
    # Use candidate as nonce, must be 8 bytes long!
    if len(candidate) != 8:
        return
    cipher = Salsa20.new(akey,nonce=candidate)
    plaintext = cipher.decrypt(aMessage)
    if all(c in string.printable for c in plaintext):
        print "===============================", "Salsa20", "==============================="
        print "candidate:", candidate
        print "plaintext:", plaintext
        exit(0)

# RC2 cypher
def testARC2(candidate):
    # 1. ECB no requirements
    aCypher = ARC2.new(aKey, ARC2.MODE_ECB)
    plaintext = aCypher.decrypt(aMessage)
    if all(c in string.printable for c in plaintext):
        print "===============================", "RC2 ECB", "==============================="
        print "plaintext:", plaintext
        exit(0)
    # iv for MODE_CBC, MODE_CFB, and MODE_OFB it must be 8 bytes long.
    if len(candidate) == 8:
        # 2. CBC mode with aKey being the secret key and candidate the iv
        aCypher = ARC2.new(aKey, ARC2.MODE_CBC, iv=candidate)
        plaintext = aCypher.decrypt(aMessage)
        if all(c in string.printable for c in plaintext):
            print "===============================", "RC2 MODE_CBC", "==============================="
            print "plaintext:", plaintext
            print "candidate:", candidate
            exit(0)
        # 3. CFB mode
        aCypher = ARC2.new(aKey, ARC2.MODE_CFB, iv=candidate)
        plaintext = aCypher.decrypt(aMessage)
        if all(c in string.printable for c in plaintext):
            print "===============================", "RC2 MODE_CFB", "==============================="
            print "plaintext:", plaintext
            print "candidate:", candidate
            exit(0)
        # 4. OFB mode
        aCypher = ARC2.new(aKey, ARC2.MODE_OFB, iv=candidate)
        plaintext = aCypher.decrypt(aMessage)
        if all(c in string.printable for c in plaintext):
            print "===============================", "RC2 MODE_OFB", "==============================="
            print "plaintext:", plaintext
            print "candidate:", candidate
            exit(0)
    # 5. EAX mode
    aCypher = ARC2.new(aKey, ARC2.MODE_EAX, nonce=candidate)
    plaintext = aCypher.decrypt(aMessage)
    if all(c in string.printable for c in plaintext):
        print "===============================", "RC2 MODE_EAX", "==============================="
        print "plaintext:", plaintext
        print "candidate:", candidate
        exit(0)

    
def testBlowfish(candidate):
    aCypher = Blowfish.new(aKey, Blowfish.MODE_ECB)
    plaintext = aCypher.decrypt(aMessage)
    if all(c in string.printable for c in plaintext):
        print "===============================", "BLOWFISH MODE_ECB", "==============================="
        print "plaintext:", plaintext
        print "candidate:", candidate
        exit(0)
    # iv for MODE_CBC, MODE_CFB, and MODE_OFB it must be 8 bytes long.
    if len(candidate) == 8:
        # 2. CBC mode with aKey being the secret key and candidate the iv
        aCypher = Blowfish.new(aKey, Blowfish.MODE_CBC, iv=candidate)
        plaintext = aCypher.decrypt(aMessage)
        if all(c in string.printable for c in plaintext):
            print "===============================", "Blowfish MODE_CBC", "==============================="
            print "plaintext:", plaintext
            print "candidate:", candidate
            exit(0)
        # 3. CFB mode
        aCypher = Blowfish.new(aKey, Blowfish.MODE_CFB, iv=candidate)
        plaintext = aCypher.decrypt(aMessage)
        if all(c in string.printable for c in plaintext):
            print "===============================", "Blowfish MODE_CFB", "==============================="
            print "plaintext:", plaintext
            print "candidate:", candidate
            exit(0)
        # 4. OFB mode
        aCypher = Blowfish.new(aKey, Blowfish.MODE_OFB, iv=candidate)
        plaintext = aCypher.decrypt(aMessage)
        if all(c in string.printable for c in plaintext):
            print "===============================", "Blowfish MODE_OFB", "==============================="
            print "plaintext:", plaintext
            print "candidate:", candidate
            exit(0)
    # 5. EAX mode
    aCypher = Blowfish.new(aKey, Blowfish.MODE_EAX, nonce=candidate)
    plaintext = aCypher.decrypt(aMessage)
    if all(c in string.printable for c in plaintext):
        print "===============================", "Blowfish MODE_EAX", "==============================="
        print "plaintext:", plaintext
        print "candidate:", candidate
        exit(0)


# DES3 in 2TDES mode (16 byte key)
def test3DES():
    try:
        key = DES3.adjust_key_parity(aKey)
    except ValueError:
        print "VALUE ERROR"
        exit(2)
    print "3DES Key has valid parity"
    aCypher = DES3.new(key, DES3.MODE_ECB)
    plaintext = aCypher.decrypt(aMessage)
    if all(c in string.printable for c in plaintext):
        print "===============================", "3DES", "==============================="
        print "plaintext:", plaintext
        print "candidate:", candidate
        exit(0)
# ARC4
def testARC4():
    aCypher = ARC4.new(aKey)
    plaintext = aCypher.decrypt(aMessage)
    if all(c in string.printable for c in plaintext):
        print "===============================", "ARC4", "==============================="
        print "plaintext:", plaintext
        print "candidate:", candidate
        exit(0)


# change this to point to actual file
fh = open('/Users/ivol/Uni/Crypto/crackstation-human-only.txt', 'r')
# Tests
test3DES()
testARC4()
# Set this to whichever part of a file you would like to process. i.e if you have 100 lines of input text and you would like
# to process lines 40 to 60, set aMin = 40 and aMax = 60
aMin = 0
aMax = 10000000
# modulus for the print operation: comes in hande when working with millions of lines
aModulus = 100000
aCounter = 0
for aItem in fh:
    aCounter += 1
    if (aCounter < aMin):
        continue
    elif (aCounter > aMax):
        print "Processed maximum number of input words. aMax=", aMax
        exit(0)
    aCandidate = bytes(aItem)
    testBLAKE2b(aCandidate)
    testAES(aCandidate)
    testARC2(aCandidate)
    testBlowfish(aCandidate)
    # counter is set to print line with updated value every aModulus iterations
    if (aCounter % aModulus == 0):
        print "===============================", aCounter, "words tested", "==============================="


